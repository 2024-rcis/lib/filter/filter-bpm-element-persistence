package de.ubt.ai4.petter.recpro.lib.filter.filterbpmelementpersistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilterBpmElementPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilterBpmElementPersistenceApplication.class, args);
	}

}
