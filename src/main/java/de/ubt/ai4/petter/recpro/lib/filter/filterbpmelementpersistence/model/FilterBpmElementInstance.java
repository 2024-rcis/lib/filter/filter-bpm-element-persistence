package de.ubt.ai4.petter.recpro.lib.filter.filterbpmelementpersistence.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class FilterBpmElementInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String recproElementId;
    private FilterBpmElementState state;

    public FilterBpmElementInstance copy() {
        FilterBpmElementInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }

    public static List<FilterBpmElementInstance> copy(List<FilterBpmElementInstance> instances) {
        return instances.stream().map(instance -> instance.copy()).toList();
    }
}
