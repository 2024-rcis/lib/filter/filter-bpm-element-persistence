package de.ubt.ai4.petter.recpro.lib.filter.filterbpmelementpersistence.model;

public enum FilterBpmElementState {
    TRUE,
    FALSE,
    NEUTRAL
}
